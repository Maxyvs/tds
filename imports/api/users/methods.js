import { Meteor } from 'meteor/meteor'

// Metodos para la manipulacion de datos de los usuarios
// User data manipulation methods
const methods = {

    // Metodo que añade un usuario a partir de un nombre, contraseña, email y un perfil
    // Method that adds an user by username, password, email and a profile
    'add-user'({username, password, email}) {

        // Perfil del usuario
        // User profile
        let profile = {
            wallet: 0.00,     // Dinero - Money
            orders: [],       // Pedidos - Orders
            role: 'customer'  // Rol - Role
        };

        // Crea un nuevo usuario asignandole un perfil
        // Creates a new user assigning a profile
        let id = Accounts.createUser({username: username,
            password: password,
            email: email,
            profile: profile});

        // Se le añade el rol de cliente al usuario
        // Adds the customer role to the user
        Roles.addUsersToRoles(id, 'customer');
    },

    'remove-user'({userid}) {
        Meteor.users.remove(userid);
    },

    'get-current-user'({}) {
        return Meteor.user();
    },

    'get-user-orders'({}) {
        return Meteor.user().profile.orders;
    },

    'update-user-wallet'({username, wallet}) {
        let userid = Meteor.users.find({username: username}).fetch()[0]._id;

        wallet = Number(wallet).toFixed(2);

        Meteor.users.update({_id: userid}, {$set: {'profile.wallet': wallet}});
    },

    'update-user-role'({username, role}) {
        let userid = Meteor.users.find({username: username}).fetch()[0]._id;
        Meteor.users.update({_id: userid}, {$set: {'profile.role': role}});
    }
};

// Carga de metodos
// Methods load
Meteor.methods(methods);