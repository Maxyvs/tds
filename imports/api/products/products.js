import { Mongo } from 'meteor/mongo'

// Creacion de la coleccion de productos
// Products collection creation
export const Products = new Mongo.Collection('products');