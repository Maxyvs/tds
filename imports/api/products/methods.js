import { Meteor } from 'meteor/meteor'
import { Products } from './products'

// Metodos para la manipulacion de datos de los productos
// Product data manipulation methods
const methods = {

    // Añade un producto a la coleccion de productos
    // Adds a product to the products collection
    'add-product'({product}) {
        Products.insert({
            productName: product.productName,
            description: product.description,
            imgUrl: product.imgUrl,
            category: product.category,
            price: product.price,
            buyCount: 0,
            inSale: false,
            createdAt: new Date()
        });
    },

    'delete-product'({productId}) {
        Products.remove(productId);
    },

    'get-products'({}) {
        return Products.find().fetch();
    },

    'make-order'({userId, shoppingCart, total}) {
        let userOrders = Meteor.user().profile.orders,
            wallet = Meteor.user().profile.wallet;

        userOrders.push(shoppingCart);

        wallet -= Number(total).toFixed(2);

        wallet = Number(wallet).toFixed(2);

        Meteor.users.update({_id: userId}, {$set: {
            'profile.orders': userOrders,
            'profile.wallet': wallet
        }});

        shoppingCart.forEach((product) => {
            let buys = Products.find({_id: product.productId}).fetch()[0].buyCount;

            buys = Number(buys);

            buys += Number(product.quantity);

            Products.update({_id: product.productId}, {$set: {buyCount: buys}});
        });
    },

    'get-products-by-category'({category}) {
        return Products.find({category: category}).fetch();
    },

    'get-products-in-sale'({}) {
        return Products.find({inSale: true}).fetch();
    },

    'get-product-info'({productId}) {
      return Products.find({_id: productId}).fetch();
    },

    'edit-product'({product}) {
        Products.update({_id: product.productId}, { $set: {
                productName: product.productName,
                description: product.description,
                imgUrl: product.imgUrl,
                price: product.price,
                inSale: product.inSale
            }
        });
    }
};

// Carga de metodos
// Methods load
Meteor.methods(methods);