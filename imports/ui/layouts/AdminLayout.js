import React, { Component } from 'react'

import AdminSideBar from '../pages/admin/AdminSideBar'

export default class AdminLayout extends Component {
    render() {
        return (
            <div className="admin">
                <div className="row margin-0">
                    <div className="col s2 sidebar">
                        <AdminSideBar />
                    </div>
                    <div className="col s10 border-left">
                        {this.props.content}
                    </div>
                </div>
            </div>
        )
    }
}