import React, { Component } from 'react'

import Header from '../components/Header'
import Footer from '../components/Footer'
import { Session } from 'meteor/session'

export default class AppLayout extends Component {
    render() {
        if (localStorage.getItem("shoppingCart") != null && localStorage.getItem("shoppingCart").length > 0) {
            Session.set("shoppingCart", JSON.parse(localStorage.getItem("shoppingCart")));
        } else {
            Session.set("shoppingCart", []);
        }
        return (
            <div>
                <Header />
                {this.props.content}
                <Footer />
            </div>
        )
    }
}