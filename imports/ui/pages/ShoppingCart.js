import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Session } from 'meteor/session'

import CartItem from './shopping-cart/CartItem'

export default class ShoppingCart extends Component {
    constructor() {
        super();

        this.state = {
            shoppingCart: [],
            total: 0.00
        };
    }

    makeOrder() {
        if (Meteor.user()) {
            if (this.state.shoppingCart != null && this.state.shoppingCart.length > 0) {
                Meteor.call('make-order', ({
                    userId: Meteor.userId(),
                    shoppingCart: this.state.shoppingCart,
                    total: this.state.total
                }), (err, res) => {
                    if (!err) {
                        M.toast({html: 'Compra realizada con exito'});
                        this.emptyCart();
                    } else {
                        M.toast({html: 'Ha habido algun problema en la compra'});
                    }
                });
            } else {
                M.toast({html: 'Tienes que añadir productos al carrito para poder comprar'});
            }
        } else {
            M.toast({html: 'Tienes que iniciar sesion primero para realizar un pedido'});
        }
    }

    emptyCart() {
        if (this.state.shoppingCart != null && this.state.shoppingCart.length > 0) {
            localStorage.setItem("shoppingCart", []);
            this.setState({shoppingCart: [], total: 0.00});
        } else {
            M.toast({html: 'El carrito ya esta vacio'});
        }
    }

    componentWillMount() {
        let shoppingCart = Session.get("shoppingCart");

        if (shoppingCart != null && shoppingCart.length > 0) {
            let total = 0.00;

            shoppingCart.forEach((product) => {
                total += Number(product.price);
            });

            total = Number(total).toFixed(2);

            this.setState({
                shoppingCart: shoppingCart,
                total: total
            })
        }
    }

    render() {
        return (
            <div className="shopping-cart">
                <div className="row">
                    <div className="col s12 center">
                        <Link to="/" className="btn-large waves-effect waves-light right">Volver al catalogo</Link>
                        <h4>Carrito</h4>
                    </div>
                    <div className="container">
                        <div className="col s12">
                            {this.state.shoppingCart.map((cartItem, index) => {
                                return (<CartItem key={index} {...cartItem} />)
                            })}
                        </div>
                        <div className="col s12">
                            <a onClick={() => {this.makeOrder()}} className="btn-large waves-effect waves-light right">Comprar</a>
                            <a onClick={() => {this.emptyCart()}} className="btn-large waves-effect waves-light right">Vaciar carrito</a>
                            <h5>Total:</h5>
                            <h5>{this.state.total}€</h5>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}