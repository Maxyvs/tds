import React, { Component } from 'react'
import {Session} from "meteor/session";

export default class CartItem extends Component {

    removeItemFromShoppingCart() {
        let shoppingCart = Session.get("shoppingCart"),
            productToRemoveIndex = -1;

        if (shoppingCart != null) {

            shoppingCart.forEach((product, index) => {
                let productId = product.productId;

                if (productId != null && productId == this.props.productId) {
                    if (product.quantity > 1) {
                        product.price = product.price - (product.price / product.quantity);
                        product.quantity--;
                    } else {
                        productToRemoveIndex = index;
                    }
                }
            });

            if (productToRemoveIndex != -1) {
                shoppingCart.splice(productToRemoveIndex, 1);
            }

            localStorage.setItem("shoppingCart", JSON.stringify(shoppingCart));
            Session.set("shoppingCart", JSON.parse(localStorage.getItem("shoppingCart")));
        }
    }

    render() {
        return (
            <div className="row card blue-grey darken-1">

                <div className="col s3">
                    <div className="card-content white-text">
                        <img className="border-radius responsive-img" src={this.props.imgUrl} alt="cart-item"/>
                    </div>
                </div>
                <div className="col s7">
                    <div className="card-content white-text">
                        <span className="card-title">{this.props.productName} x {this.props.quantity}</span>
                        <hr/>
                        <p>{this.props.description}</p>
                        <hr/>
                        <span>Precio: {this.props.price}€</span>
                    </div>
                </div>
                <div className="col s2">
                    <div className="card-content white-text">
                        <a onClick={() => {this.removeItemFromShoppingCart()}} href="/cart" className="btn-floating btn-large waves-effect waves-light red"><i
                            className="material-icons">remove</i></a>
                    </div>
                </div>
            </div>
        )
    }
}