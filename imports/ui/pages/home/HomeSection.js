import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import Catalog from './Catalog'

export default class HomeSection extends Component {
    render() {
        return (
            <section className="home-section">
                <div className="row">
                    <div className="col s4 m4 l9 center">
                        <h4>Catalogo</h4>
                    </div>
                    <div className="col s8 m8 l3">
                        <div className="right">
                            <Link to="/cart" className="waves-effect waves-light btn-flat"><i
                                className="material-icons">add_shopping_cart</i></Link>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col s12">
                        <Catalog />
                    </div>
                </div>
            </section>
        )
    }
}