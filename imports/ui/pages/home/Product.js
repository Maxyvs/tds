import React, { Component } from 'react'
import { Session } from 'meteor/session'

export default class Product extends Component {

    addToCart() {

        let shoppingCart = Session.get("shoppingCart"),
            alreadyInCart = false;

        if (shoppingCart != null && shoppingCart.length > 0) {

            shoppingCart.forEach((product) => {
                let productId = product.productId;

                if (productId != null && productId == this.props._id) {
                    alreadyInCart = true;
                    product.quantity++;
                    product.price = this.props.price * product.quantity;
                }
            });
            if (!alreadyInCart) {
                shoppingCart.push({
                    productId: this.props._id,
                    productName: this.props.productName,
                    description: this.props.description,
                    imgUrl: this.props.imgUrl,
                    price: this.props.price,
                    quantity: 1
                });
            }

            localStorage.setItem("shoppingCart", JSON.stringify(shoppingCart));
            Session.set("shoppingCart", JSON.parse(localStorage.getItem("shoppingCart")));
        } else {
            shoppingCart = [{
                productId: this.props._id,
                productName: this.props.productName,
                description: this.props.description,
                imgUrl: this.props.imgUrl,
                price: this.props.price,
                quantity: 1
            }];

            localStorage.setItem("shoppingCart", JSON.stringify(shoppingCart));
            Session.set("shoppingCart", JSON.parse(localStorage.getItem("shoppingCart")));
        }

        M.toast({html: 'Añadido al carrito', classes: 'rounded'});
    }

    render() {
        return (
            <div className="col l3 s4">
                <div className="card horizontal large blue-grey darken-1 center">
                    <div className="card-content white-text">
                        <img className="responsive-img center-block max-width-50 border-radius center" src={this.props.imgUrl} alt="producto"/>
                        <hr/>
                        <span className="card-title">{this.props.productName}</span>
                        <hr/>
                        <p className="font-size-12">{this.props.description}</p>
                        <hr/>
                        <p className="white-text">{this.props.price}€</p>
                        <hr/>
                    </div>
                    <div className="card-action">
                        <a className="margin-0" href="#" onClick={() => {this.addToCart()}}>Añadir</a>
                    </div>
                </div>
            </div>
        )
    }
}