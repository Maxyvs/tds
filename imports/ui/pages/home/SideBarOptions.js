import React, { Component } from 'react'

export default class SidebarOptions extends Component {
    render() {
        return [
            <li key="1">Todas</li>,
            <li key="2">Moviles</li>,
            <li key="3">USB</li>,
            <li key="4">Tarjetas SD</li>,
            <li key="5">Power banks</li>
        ]
    }
}