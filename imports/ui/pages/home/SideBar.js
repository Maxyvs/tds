import React, { Component } from 'react'

import SidebarOptions from './SideBarOptions'

export default class SideBar extends Component {
    render() {
        return (
            <ul>
                <p>Categorias</p>
                <SidebarOptions />
            </ul>
        )
    }
}