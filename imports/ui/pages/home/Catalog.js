import React, { Component } from 'react'

import Product from './Product'

export default class Catalog extends Component {
    constructor() {
        super();

        this.state = {
            products: []
        }
    }

    componentWillMount() {
        Meteor.call('get-products', ({}) , (err, res) => {
            this.setState({products: res});
        })
    }

    render() {
        return (
            <section className="catalog">
                <div className="row">
                    {this.state.products.map((product, index) => {
                        return (<Product key={index} {...product} />)
                    })}
                </div>
            </section>
        )
    }
}