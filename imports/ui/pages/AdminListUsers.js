import React, { Component } from 'react'

import AdminLayout from '../layouts/AdminLayout'

import ListUsers from './admin/ListUsers'

export default class AdminListUsers extends Component {
    render() {
        return (
            <AdminLayout content={ <ListUsers /> }/>
        )
    }
}