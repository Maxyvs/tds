import React, { Component } from 'react'
import TrackerReact from 'meteor/ultimatejs:tracker-react'
import { Link } from 'react-router-dom'
import { Meteor } from 'meteor/meteor'

export default class Logout extends TrackerReact(Component) {
    logout() {
        Meteor.logout();
        window.location.href = "/";
    }

    render() {
        return (
            <div className="logout">
                <div className="container">
                    <div className="row">
                        <div className="col s12 center">
                            <h5>¿Estas seguro que deseas cerrar la sesion?</h5>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s12">
                            <div className="row">
                                <div className="col">
                                    <button onClick={this.logout} className="waves-effect waves-light btn-large left">Cerrar sesion</button>
                                </div>
                                <div className="col right">
                                    <Link to="/" className="waves-effect waves-light btn-large">Volver</Link>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}