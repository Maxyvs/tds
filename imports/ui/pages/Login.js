import React, { Component } from 'react'
import TrackerReact from 'meteor/ultimatejs:tracker-react'
import { Link } from 'react-router-dom'
import { Meteor } from 'meteor/meteor'

export default class Login extends TrackerReact(Component) {
    handleSubmit(evt) {
        evt.preventDefault();

        let user = this.refs.username.value,
            password = this.refs.password.value;

        Meteor.loginWithPassword(user, password, (err) => {
            if (err) {
                switch (err.reason) {
                    case "User not found": $("#err").text("Ese usuario no existe.");
                        break;
                    case "Incorrect password": $("#err").text("Contraseña incorrecta.");
                        break;
                    default: $("#err").text("Ha ocurrido un error.");
                }
            } else {
                window.location.href = "/";
            }
        });
    }

    render() {
        return (
            <div className="login">
                <div className="container">
                    <div className="row">
                        <div className="col s12">
                            <h5>Inicia sesion</h5>
                        </div>
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <div className="row">
                                <div className="col s12">
                                    <div className="row">
                                        <div className="input-field col s6">
                                            <i className="material-icons prefix">account_circle</i>
                                            <input maxLength="20" required id="username" type="text" ref="username" className="validate" />
                                            <label htmlFor="username">Nombre de usuario</label>
                                        </div>
                                        <div className="input-field col s6">
                                            <i className="material-icons prefix">vpn_key</i>
                                            <input maxLength="30" required id="password" type="password" ref="password" className="validate" />
                                            <label htmlFor="password">Contraseña</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           <div className="row">
                               <div className="col">
                                   <button type="submit" className="waves-effect waves-light btn-large left">Entrar </button>
                               </div>
                               <div className="col">
                                   <p className="red-text" id="err"></p>
                                   <p className="green-text" id="success"></p>
                               </div>
                               <div className="col right">
                                   <Link to="/register" className="waves-effect waves-light btn-large">Crear cuenta</Link>
                               </div>
                           </div>
                        </form>

                    </div>
                </div>
            </div>
        )
    }
}