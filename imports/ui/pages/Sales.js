import React, { Component } from 'react'

import ProductInSale from './home/ProductInSale'
import {Link} from "react-router-dom";

export default class Sales extends Component {
    constructor() {
        super();

        this.state = {
            products: []
        }
    }

    componentWillMount() {
        Meteor.call('get-products-in-sale', ({}) , (err, res) => {
            this.setState({products: res});
        });
    }

    render() {
        return (
            <div className="container">
                <section className="products-in-sale">
                    <div className="col s4 m4 l9 center">
                        <h4>Productos en oferta</h4>
                    </div>
                    <div className="col s8 m8 l3">
                        <div className="right">
                            <Link to="/cart" className="waves-effect waves-light btn-flat"><i
                                className="material-icons">add_shopping_cart</i></Link>
                        </div>
                    </div>
                    <div className="row">
                        {this.state.products.map((product, index) => {
                            return (<ProductInSale key={index} {...product} />)
                        })}
                    </div>
                </section>
            </div>

        )
    }
}