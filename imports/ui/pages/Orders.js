import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {Meteor} from "meteor/meteor";

import Order from './orders/Order'

export default class Orders extends Component {
    constructor() {
        super();

        this.state = {
            orders: []
        };
    }

    componentWillMount() {
        let orders;
        Meteor.call('get-user-orders', {}, (err, res) => {
            if (res) {
                orders = res[0];

            }
        });


        if (orders != null) {
            this.setState({ orders: orders });
        }
    }

    render() {
        return (
            <div className="container">
                <div className="orders">
                    <div className="row">
                        <div className="col s12 center">
                            <Link to="/profile" className="btn-large waves-effect waves-light right">Volver</Link>
                            <h4>Pedidos</h4>
                        </div>

                            {this.state.orders.map((order, index) => {
                                console.log(order);
                                return (<Order key={index} {...order} />)
                            })}
                    </div>
                </div>
            </div>
        )
    }
}