import React, { Component } from 'react'

export default class Product extends Component {

    render() {
        return (
            <li className="collection-item avatar">
                <img src={this.props.imgUrl} alt="" className="circle" />
                <span className="title">{this.props.productName}</span>
                <p>Cantidad: {this.props.quantity} <br/>
                    {this.props.price}
                </p>
            </li>
        )
    }
}