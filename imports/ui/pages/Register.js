import React, { Component } from 'react'
import { Meteor } from 'meteor/meteor'

export default class Register extends Component {
    handleSubmit(evt) {
        evt.preventDefault();
        $("#err").text("");
        $("#success").text("");

        let username = this.refs.username.value,
            email = this.refs.email.value,
            password = this.refs.password.value,
            confirmPassword = this.refs.confirmPassword.value;

        if (password === confirmPassword) {
            Meteor.call('add-user',
                {username: username,
                    password: password,
                    email: email
                }, (err) => {
                    if (err) {
                        switch (err.reason) {
                            case "Username already exists.": $("#err").text("Ese usuario ya existe.");
                                break;
                            case "Email already exists.": $("#err").text("Ese email ya existe.");
                                break;
                            default: $("#err").text("Ha ocurrido un error.");
                        }
                    } else {
                        $("#success").text("Usuario creado correctamente, ya puede iniciar sesion.");
                    }
                });
            evt.target.reset();
        } else {
            $("#err").text("Las contraseñas tienen que ser iguales.");
        }
    }

    render() {
        return (
            <div className="login">
                <div className="container">
                    <div className="row">
                        <div className="col s12">
                            <h5>Registro</h5>
                        </div>
                        <form id="loginForm" className="col s12" onSubmit={this.handleSubmit.bind(this)}>
                            <div className="row">
                                <div className="input-field col s6">
                                    <i className="material-icons prefix">account_circle</i>
                                    <input required maxLength="20" ref="username" id="username" type="text" className="validate" />
                                    <label htmlFor="username">Nombre de usuario</label>
                                </div>
                                <div className="input-field col s6">
                                    <i className="material-icons prefix">email</i>
                                    <input required maxLength="40" ref="email" id="email" type="email" className="validate" />
                                    <label htmlFor="email">Email</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s6">
                                    <i className="material-icons prefix">vpn_key</i>
                                    <input required maxLength="30" ref="password" id="password" type="password" className="validate" />
                                    <label htmlFor="password">Contraseña</label>
                                </div>
                                <div className="input-field col s6">
                                    <i className="material-icons prefix">vpn_key</i>
                                    <input required maxLength="30" ref="confirmPassword" id="confirm-password" type="password" className="validate" />
                                    <label htmlFor="confirm-password">Confirmar contraseña</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <button type="submit" className="waves-effect waves-light btn-large left">Registrarse</button>
                                </div>
                                <div className="col">
                                    <p className="red-text" id="err"></p>
                                    <p className="green-text" id="success"></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}