import React, { Component } from 'react'

import AdminLayout from '../layouts/AdminLayout'
import AdminSection from './admin/AdminSection'

export default class Admin extends Component {
    render() {
        return (
            <AdminLayout content={ <AdminSection /> }/>
        )
    }
}