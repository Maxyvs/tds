import React, { Component } from 'react'

import SideBar from './home/SideBar'
import HomeSection from './home/HomeSection'

export default class Home extends Component {
    render() {
        return (
            <div className="home">
                <div className="row margin-0">
                    <div className="col s12">
                        <HomeSection />
                    </div>
                </div>
            </div>
        )
    }
}