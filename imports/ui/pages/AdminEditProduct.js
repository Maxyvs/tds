import React, { Component } from 'react'

import AdminLayout from '../layouts/AdminLayout'

import EditProduct from './admin/EditProduct'

export default class AdminEditProduct extends Component {
    render() {
        return (
            <AdminLayout content={ <EditProduct /> }/>
        )
    }
}