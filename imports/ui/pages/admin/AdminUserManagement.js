import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class AdminUserManagement extends Component {
    render() {
        return (
            <div className="admin-user">
                <h5>Usuarios</h5>
                <Link to="/list-users" className="waves-effect waves-light btn-large">Listar usuarios</Link>
            </div>
        )
    }
}