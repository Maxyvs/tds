import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class AdminProductManagement extends Component {
    render() {
        return (
            <div className="admin-product">
                <h5>Productos</h5>
                <Link to="/add-product" className="waves-effect waves-light btn-large">Añadir producto</Link>
                <Link to="/edit-product" className="waves-effect waves-light btn-large">Editar producto</Link>
                <Link to="/list-products" className="waves-effect waves-light btn-large">Listar productos</Link>
            </div>
        )
    }
}