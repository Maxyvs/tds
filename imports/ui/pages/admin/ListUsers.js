import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Meteor } from 'meteor/meteor'
import TrackerReact from 'meteor/ultimatejs:tracker-react'

import User from '../admin/User'

export default class ListUsers extends TrackerReact(Component) {

    constructor() {
        super();
        this.state = {
            usersList: [],
            loading: true
        }
    }

    updateUserRole() {
        let username = this.refs.username.value,
            role = this.refs.role.value;
        Meteor.call('update-user-role', {username: username, role: role});
    }

    render() {
        let users = Meteor.users.find().fetch();
        if (this.state.loading == true) {
            if (users.length > 0) {
                this.setState({usersList: users, loading: false});
            }
        }
        return (
            <div>
                <div className="row">
                    <div className="col offset-s2 s8 center-align">
                        <h4>Usuarios</h4>
                        <table>
                            <thead>
                            <tr>
                                <th>Nombre de usuario</th>
                                <th>Rol</th>
                                <th className="right">Eliminar</th>
                            </tr>
                            </thead>

                            <tbody>
                            {this.state.usersList.map((user, index) => {
                                return (<User key={index} {...user} />)
                            })}
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr/>
                <div className="row">
                    <div className="col s12 center">
                        <h5>Cambiar rol a usuario</h5>
                    </div>
                    <form className="col s12">
                        <div className="input-field col s6">
                            <input ref="username" required maxLength="20" placeholder="Nombre de usuario" id="username" type="text" className="validate"/>
                        </div>
                        <div className="input-field col s6">
                            <select ref="role" className="browser-default">
                                <option value="customer">Cliente</option>
                                <option value="admin">Administrador</option>
                            </select>
                        </div>
                        <div className="col s12">
                            <button onClick={() => {this.updateUserRole()}} className="waves-effect waves-light btn-large left">Cambiar rol </button>
                        </div>
                    </form>
                </div>
                <div className="row">
                    <div className="col s12 center">
                        <Link to="/admin" className="waves-effect waves-light btn-large">Volver</Link>
                    </div>
                </div>
            </div>

        )
    }
}