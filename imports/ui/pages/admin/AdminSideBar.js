import React, { Component } from 'react'

import AdminSidebarOptions from './AdminSideBarOptions'

export default class AdminSideBar extends Component {
    render() {
        return (
            <ul>
                <p>Administracion</p>
                <AdminSidebarOptions />
            </ul>
        )
    }
}