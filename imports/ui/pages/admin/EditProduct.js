import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import TrackerReact from 'meteor/ultimatejs:tracker-react'
import { Meteor } from 'meteor/meteor'
import { Products } from '../../../api/products/products'

export default class EditProduct extends TrackerReact(Component) {
    /*
    constructor() {
        super();
        this.state = {
            productsList: [],
            loading: true
        }
    }
*/
    updateProduct() {
        let inSale;

        if ($("#inSale")[0].checked) {
            inSale = true;
        } else {
            inSale = false;
        }

        let product = {
            productId: this.refs.productId.value,
            productName: this.refs.productName.value,
            description: this.refs.description.value,
            imgUrl: this.refs.imgUrl.value,
            price: this.refs.price.value,
            inSale: inSale
        };

        console.log(product);

        Meteor.call('edit-product', {product}, (err) => {
            if (err) {
                $("#err").text("Ha ocurrido un error al editar el producto.");
            } else {
                $("#info").text("Se ha editado el producto correctamente.");
            }
        });

        $("#edit-product-form")[0].reset();
    }

    getProductInfo() {
        let productId = this.refs.productId.value,
            product;

        Meteor.call('get-product-info', {productId: productId}, (err, res) => {
            if (err) {
                $("#err").text("Ha habido algun tipo de error al obtener el producto");
            }else if (res) {
                product = res[0];

                if (product != null) {
                    $("#err").text("");

                    $("#productName")[0].value = product.productName;
                    $("#description")[0].value = product.description;
                    $("#imgUrl")[0].value = product.imgUrl;
                    $("#price")[0].value = product.price;

                    if (product.inSale) {
                        $("#inSale")[0].checked = true;
                    } else {
                        $("#notInSale")[0].checked = true;
                    }
                } else {
                    $("#err").text("No se ha encontrado un producta con esa id");
                }

            }
        });
        /*
        let exit = false,
            i = 0,
            idToFind = this.refs.productId.value;

        if (idToFind != "" && this.state.productsList.length > 0) {
            do {
                if (this.state.productsList[i] != null) {
                    if (idToFind == this.state.productsList[i]._id) {

                        $("#productName")[0].value = this.state.productsList[i].productName;
                        $("#description")[0].value = this.state.productsList[i].description;
                        $("#imgUrl")[0].value = this.state.productsList[i].imgUrl;

                        if (this.state.productsList[i].inSale) {
                            $("#inSale")[0].checked = true;
                        } else {
                            $("#notInSale")[0].checked = true;
                        }

                        exit = true;
                    } else {
                        i++;
                    }
                } else {
                    exit = true;
                }
            } while (!exit);
        }
        */
    }

    render() {
        /*
        let products = Products.find().fetch();
        if (this.state.loading == true) {
            if (products.length > 0) {
                this.setState({productsList: products, loading: false});
            }
        }
        */
        return (
            <div className="row">
                <div className="col s12 center">
                    <h5>Editar producto</h5>
                </div>
                <form id="edit-product-form" className="col s12">
                    <div className="row">
                        <div className="input-field col s6">
                            <i className="material-icons prefix">grade</i>
                            <input ref="productId" required id="productId" type="text" className="validate" />
                            <label htmlFor="productId">Id del producto (obligatoria)</label>
                        </div>
                        <div className="col s6">
                            <button onClick={() => {this.getProductInfo()}} className="waves-effect waves-light btn-large">Obtener datos del producto</button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <i className="material-icons prefix">title</i>
                            <input ref="productName" required id="productName" type="text" className="validate" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s12">
                            <i className="material-icons prefix">description</i>
                            <textarea ref="description" required id="description" className="materialize-textarea validate" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field input-field col s12">
                            <i className="material-icons prefix">image</i>
                            <input ref="imgUrl" required id="imgUrl" type="text" className="validate" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="input-field col s6">
                            <div className="row">
                                <div className="col offset-s1">
                                    <p>Poner en oferta</p>
                                </div>
                                <div className="col">
                                    <p>
                                        <label>
                                            <input id="inSale" className="with-gap" name="inSale" type="radio" value="yes"/>
                                            <span>Si</span>
                                        </label>
                                    </p>
                                </div>
                                <div className="col">
                                    <p>
                                        <label>
                                            <input id="notInSale" className="with-gap" name="inSale" type="radio" value="no"/>
                                            <span>No</span>
                                        </label>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="input-field col s6">
                            <i className="material-icons prefix">attach_money</i>
                            <input ref="price" id="price" type="text" step=".01" className="validate" />
                        </div>
                    </div>
                </form>
                <div className="col s12">
                    <div className="row">
                        <div className="col">
                            <button onClick={() => {this.updateProduct()}} id="edit-product-submit" className="waves-effect waves-light btn-large">Aceptar</button>
                        </div>
                        <div className="col">
                            <p id="err" className="red-text"></p>
                            <p id="info" className="green-text"></p>
                        </div>
                        <div className="col right align-right">
                            <Link to="/admin" className="waves-effect waves-light btn-large">Volver</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}