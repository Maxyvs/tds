import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class AddProduct extends Component {
    addProduct() {
        let product = {
            productName: this.refs.productName.value,
            description: this.refs.description.value,
            imgUrl:      this.refs.imgUrl.value,
            category:    this.refs.category.value,
            price:       this.refs.price.value
        };

        Meteor.call('add-product', { product }, (err) => {
            if (err) {
                $("#err").text("Ha ocurrido un error al insertar el producto.");
            } else {
                $("#info").text("Se ha insertado el producto correctamente.");
            }
        });

        $("#add-product-form")[0].reset();
    }

    render() {
        return (
            <div className="add-product">
                <div className="container">
                    <div className="row">
                        <div className="col s12 center">
                            <h5>Añadir producto</h5>
                        </div>
                        <form id="add-product-form" className="col s12">
                            <div className="row">
                                <div className="input-field col s12">
                                    <i className="material-icons prefix">title</i>
                                    <input ref="productName" required id="productName" type="text" className="validate" />
                                    <label htmlFor="productName">Nombre de producto</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s12">
                                    <i className="material-icons prefix">description</i>
                                    <textarea ref="description" required id="description" className="materialize-textarea validate" />
                                    <label htmlFor="description">Descripcion</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field input-field col s12">
                                    <i className="material-icons prefix">image</i>
                                    <input ref="imgUrl" required id="imgUrl" type="text" className="validate" />
                                    <label htmlFor="imgUrl">URL de la imagen</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s6">
                                    <select ref="category" className="browser-default">
                                        <option value="" disabled>Categoria</option>
                                        <option value="mobile">Movil</option>
                                        <option value="usb">USB</option>
                                        <option value="sdcard">Tarjeta SD</option>
                                        <option value="powerbank">Powerbank</option>
                                    </select>
                                </div>
                                <div className="input-field col s6">
                                    <i className="material-icons prefix">attach_money</i>
                                    <input ref="price" required id="price" step=".01" type="number" className="validate" />
                                    <label htmlFor="price">Precio</label>
                                </div>
                            </div>
                        </form>
                        <div className="col s12">
                            <div className="row">
                                <div className="col">
                                    <button onClick={() => {this.addProduct()}} id="add-product-submit" className="waves-effect waves-light btn-large">Añadir</button>
                                </div>
                                <div className="col">
                                    <p id="err" className="red-text"></p>
                                    <p id="info" className="green-text"></p>
                                </div>
                                <div className="col right align-right">
                                    <Link to="/admin" className="waves-effect waves-light btn-large">Volver</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}