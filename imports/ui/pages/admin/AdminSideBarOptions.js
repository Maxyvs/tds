import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class AdminSidebarOptions extends Component {
    render() {
        return [
            <li key="add-product"><Link to="/add-product">Añadir producto</Link></li>,
            <li key="edit-product"><Link to="/edit-product">Editar producto</Link></li>,
            <li key="list-products"><Link to="/list-products">Listar productos</Link></li>,
            <li key="list-users"><Link to="/list-users">Listar usuarios</Link></li>
        ]
    }
}