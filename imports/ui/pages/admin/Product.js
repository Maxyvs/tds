import React, { Component } from 'react'

export default class Product extends Component {
    deleteProduct() {
        Meteor.call('delete-product', {productId: this.props._id}, (err) => {
            if (!err) {
                window.location.href = '/list-products';
            }
        });
    }

    render() {
        return (
            <li className="collection-item avatar">
                <img src={this.props.imgUrl} alt="product-image" className="circle" />
                <span className="title">{this.props.productName}</span>
                <p>{this.props.description}</p>
                <p className="bold red-text">{this.props.price}€</p>
                <p>Comprados: {this.props.buyCount}</p>
                <p>ID (necesaria para editar): {this.props._id}</p>
                <a href="#" onClick={() => {this.deleteProduct()}} className="secondary-content"><i className="material-icons">delete</i></a>
            </li>
        )
    }
}