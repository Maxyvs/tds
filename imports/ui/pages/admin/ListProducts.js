import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import TrackerReact from 'meteor/ultimatejs:tracker-react'
import { Products } from '../../../api/products/products'

import Product from '../admin/Product'

export default class ListProducts extends TrackerReact(Component) {
    constructor() {
        super();
        this.state = {
            productsList: [],
            loading: true
        }
    }

    render() {
        let products = Products.find().fetch();
        if (this.state.loading == true) {
            if (products.length > 0) {
                this.setState({productsList: products, loading: false});
            }
        }
        return (
            <div className="row">
                <div className="col s12 center">
                    <h5>Productos</h5>
                </div>
                <div className="col offset-s2 s8">
                    <ul className="collection">
                        {this.state.productsList.map((product, index) => {
                            return (<Product key={index} {...product} />)
                        })}
                    </ul>
                </div>
                <div className="col s12 center">
                    <Link to="/admin" className="waves-effect waves-light btn-large">Volver</Link>
                </div>
            </div>
        )
    }
}