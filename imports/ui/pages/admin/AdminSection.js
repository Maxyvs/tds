import React, { Component } from 'react'

import AdminProductManagement from './AdminProductManagement'
import AdminUserManagement from './AdminUserManagement'

export default class AdminSection extends Component {
    render() {
        return (
            <section className="admin-section center">
                <div className="row">
                    <div className="col s12">
                        <h3>Panel de administracion</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col s6">
                        <AdminProductManagement />
                    </div>
                    <div className="col s6">
                        <AdminUserManagement />
                    </div>
                </div>
            </section>
        )
    }
}