import React, { Component } from 'react'
import TrackerReact from 'meteor/ultimatejs:tracker-react'
import { Meteor } from 'meteor/meteor'

export default class User extends TrackerReact(Component) {
    handleClick() {
        Meteor.call('remove-user', {userid: this.props._id});
        window.location.href = "/list-users";
    }

    render() {
        return (
            <tr id={this.props._id}>
                <th>{this.props.username}</th>
                <th>{this.props.profile.role}</th>
                <th><a href="#" onClick={() => {this.handleClick()}} className="secondary-content"><i className="material-icons">delete</i></a></th>
            </tr>
        )
    }
}