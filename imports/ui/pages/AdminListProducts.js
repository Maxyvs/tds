import React, { Component } from 'react'

import AdminLayout from '../layouts/AdminLayout'

import ListProducts from './admin/ListProducts'

export default class AdminListProducts extends Component {
    render() {
        return (
            <AdminLayout content={ <ListProducts /> }/>
        )
    }
}