import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Meteor } from 'meteor/meteor'

export default class Profile extends Component {
    constructor() {
        super();

        this.state = {
            user: {}
        };
    }

    componentWillMount() {
        let user;

        Meteor.call('get-current-user', {}, (err, res) => {
            if (res) {
                user = res;
                this.setState({user: user, wallet: user.profile.wallet});
            }
        });
    }

    handleSubmit(evt) {
        evt.preventDefault();

        let money = Number(this.refs.money.value),
            wallet = Number(this.state.wallet);

        wallet += money;
        console.log(wallet);

        Meteor.call('update-user-wallet', {username: this.state.user.username, wallet: wallet});

        window.location.href = "/profile";
    }

    render() {
        return (
            <div className="login">
                <div className="container">
                    <div className="row card-panel">
                        <div className="col s12">
                            <h4>Perfil</h4>
                        </div>
                        <div className="col s12">
                            <h5>Nombre de usuario: <p className="font-size-16">{this.state.user.username}</p></h5>
                            <h5>Fondos: <p className="font-size-16">{this.state.wallet}€</p></h5>
                        </div>
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <div className="row">
                                <div className="col s12">
                                    <div className="row">
                                        <div className="input-field col s12">
                                            <h5>Añadir fondos</h5>
                                        </div>
                                        <div className="input-field col s3">
                                            <i className="material-icons prefix">account_balance_wallet</i>
                                            <input maxLength="20" required id="money" type="number" step=".01" ref="money" className="validate" />
                                            <label htmlFor="money">Cantidad</label>
                                        </div>
                                        <div className="input-field col s3">
                                            <button type="submit" className="waves-effect waves-light btn-large left">Añadir </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <Link to="/" className="waves-effect waves-light btn-large">Volver</Link>
                                </div>
                                <div className="col">
                                    <p className="red-text" id="err"></p>
                                    <p className="green-text" id="success"></p>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        )
    }
}