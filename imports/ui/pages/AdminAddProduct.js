import React, { Component } from 'react'

import AdminLayout from '../layouts/AdminLayout'

import AddProduct from './admin/AddProduct'

export default class Admin extends Component {
    render() {
        return (
            <AdminLayout content={ <AddProduct /> }/>
        )
    }
}