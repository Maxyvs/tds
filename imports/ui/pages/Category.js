import React, { Component } from 'react'
import Product from './home/Product'
import {Link} from "react-router-dom";
import {Meteor} from "meteor/meteor";

export default class Category extends Component {
    constructor() {
        super();

        this.state = {
            products: []
        }
    }

    updateCategory() {
        let category = this.refs.category.value;
        Meteor.call('get-products-by-category', {category: category}, (err, res) => {
            this.setState({products: res});
        });
    }

    render() {
        return (
            <div className="container">
                <section className="products-in-sale">
                    <div className="row">
                        <div className="col s4 m8 l9 center">
                            <h4>Obtener productos por categoria</h4>
                        </div>
                        <div className="col s8 m4 l3">
                            <div className="right">
                                <Link to="/cart" className="waves-effect waves-light btn-flat"><i
                                    className="material-icons">add_shopping_cart</i></Link>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <form className="col s12">
                            <div className="input-field col s2">
                                <h5>Categoria</h5>
                            </div>
                            <div className="input-field col offset-s2 s5">
                                <select ref="category" className="browser-default">
                                    <option value="mobile">Movil</option>
                                    <option value="pendrive">Pendrive</option>
                                    <option value="sdcard">Tarjeta SD</option>
                                    <option value="powerbank">Powerbank</option>
                                </select>
                            </div>
                            <div className="col offset-s1 s2">
                                <button onClick={() => {this.updateCategory()}} className="waves-effect waves-light btn-large left">Obtener </button>
                            </div>
                        </form>
                    </div>
                    <div className="row">
                        {this.state.products.map((product, index) => {
                            return (<Product key={index} {...product} />)
                        })}
                    </div>
                </section>
            </div>

        )
    }
}