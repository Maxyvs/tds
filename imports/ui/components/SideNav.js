import React, { Component } from 'react'

import Logo from './Logo'
import NavOptions from './NavOptions'

export default class SideNav extends Component {
    render() {
        return (
            <div className="sidenav" id="nav-mobile">
                <Logo />
                <ul>
                    <NavOptions />
                </ul>
            </div>

        )
    }
}