import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import SideNav from './SideNav'
import NavOptions from './NavOptions'

export default class Header extends Component {
    render() {
        return (
            <div>
                <nav>
                    <div className="nav-wrapper">
                        <Link to="/" className="brand-logo left">TDS</Link>
                        <a href="#" data-target="nav-mobile" className="sidenav-trigger right"><i
                            className="material-icons">menu</i></a>
                        <ul className="right hide-on-med-and-down">
                            <NavOptions />
                        </ul>
                    </div>
                </nav>

                <SideNav />
            </div>
        )
    }
}