import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Meteor } from 'meteor/meteor'
import TrackerReact from 'meteor/ultimatejs:tracker-react'

export default class NavOptions extends TrackerReact(Component) {

    hideSidenav() {
        $('.sidenav').sidenav('close');
    }

    render() {
        let loggedInNav;

        if(Meteor.user()) {
            loggedInNav = [<li key="profile"><Link onClick={() => {this.hideSidenav()}} to="/profile">Mi perfil</Link></li>,
                <li key="logout"><Link onClick={() => {this.hideSidenav()}} to="/logout" className="blue-text text-accent-4">Cerrar sesion</Link></li>]
            if (Meteor.user().profile.role == 'admin') {
                loggedInNav.push(
                    <li key="admin"><Link onClick={() => {this.hideSidenav()}} to="/admin">Panel de Administracion</Link></li>
                );
            }
        } else {
            loggedInNav = <li><Link onClick={() => {this.hideSidenav()}} to="/login" className="blue-text text-accent-4">Iniciar sesion</Link></li>
        }

        return (
            <div>
                <li><Link onClick={() => {this.hideSidenav()}} to="/">Catalogo</Link></li>
                <li><Link onClick={() => {this.hideSidenav()}} to="/sales">Ofertas</Link></li>
                { loggedInNav }
            </div>
        )
    }
}