import React, { Component } from 'react'

export default class Social extends Component {
    render() {
        return (
            <div className="social">
                <a className="waves-effect waves-light btn">
                    <i className="fa fa-facebook"></i></a>
                <a className="waves-effect waves-light btn">
                    <i className="fa fa-google"></i></a>
                <a className="waves-effect waves-light btn">
                    <i className="fa fa-instagram"></i></a>
                <a className="waves-effect waves-light btn">
                    <i className="fa fa-twitter"></i></a>
            </div>
        )
    }
}