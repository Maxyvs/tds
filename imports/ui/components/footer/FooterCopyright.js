import React, { Component } from 'react'

import Social from './Social'

export default class FooterCopyright extends Component {
    render() {
        return (
            <div className="footer-copyright">
                <div className="container">
                    <div className="row">
                        <div className="col s12">
                            <p className="right">© 2018 TDS - Todos los derechos reservados</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}