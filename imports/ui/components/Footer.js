import React, { Component } from 'react'

import FooterCopyright from './footer/FooterCopyright'

export default class Footer extends Component {
    render() {
        return (
            <div>
                <hr className="margin-0" />
                <footer className="page-footer">
                    <div className="row">
                        <div className="col s3">
                            <h5>C/ Las mujeres, 12, Utrera (Sevilla)</h5>
                            <h5>Tlf: 954638464</h5>
                        </div>

                        <div className="col s7 center">
                            <h5 className="waves-effect waves-light btn-large">TDS Proyectos</h5>
                            <p>Tienda especializada en venta de dispositivos moviles, pero a su vez experta en accesorios informaticos. </p>
                        </div>

                        <div className="col s2">
                            <h5>
                                <a href="#" className="waves-effect waves-light btn-large"><i
                                    className="material-icons">keyboard_arrow_up</i></a>
                            </h5>
                        </div>
                    </div>
                    <FooterCopyright />
                </footer>
            </div>
        )
    }
}