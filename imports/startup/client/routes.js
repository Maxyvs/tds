import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

// Principal layout de la aplicacion
// Main application layout
import AppLayout from '../../ui/layouts/AppLayout'

// Paginas publicas
// Public pages
import Home from '../../ui/pages/Home'
import Sales from '../../ui/pages/Sales'
import Category from '../../ui/pages/Category'
import Login from '../../ui/pages/Login'
import Logout from '../../ui/pages/Logout'
import Register from '../../ui/pages/Register'
import Profile from '../../ui/pages/Profile'
import ShoppingCart from '../../ui/pages/ShoppingCart'

// Paginas privadas de administracion
// Private management pages
import Admin from '../../ui/pages/Admin'
import AdminAddProduct from '../../ui/pages/AdminAddProduct'
import AdminEditProduct from '../../ui/pages/AdminEditProduct'
import AdminListProducts from '../../ui/pages/AdminListProducts'
import AdminListUsers from '../../ui/pages/AdminListUsers'

// Enrutador
// Router
export const routes = (
    <Router>
        <Switch>
            /* Para cada ruta renderiza el layout principal
               y su componente principal
             */
            /* For each route renders the main layout and its
               main component
             */
            <Route exact path="/" render={(props) => <AppLayout {...props} content={ <Home /> } />} />
            <Route path="/sales" render={(props) => <AppLayout {...props} content={ <Sales /> } />} />
            <Route path="/category" render={(props) => <AppLayout {...props} content={ <Category /> } />} />
            <Route path="/login" render={(props) => <AppLayout {...props} content={ <Login /> } />} />
            <Route path="/logout" render={(props) => <AppLayout {...props} content={ <Logout /> } />} />
            <Route path="/register" render={(props) => <AppLayout {...props} content={ <Register /> } />} />
            <Route path="/profile" render={(props) => <AppLayout {...props} content={ <Profile /> } />} />
            <Route path="/cart" render={(props) => <AppLayout {...props} content={ <ShoppingCart /> } />} />
            <Route path="/admin" render={(props) => <AppLayout {...props} content={ <Admin /> } />} />
            <Route path="/add-product" render={(props) => <AppLayout {...props} content={ <AdminAddProduct /> } />} />
            <Route path="/edit-product" render={(props) => <AppLayout {...props} content={ <AdminEditProduct /> } />} />
            <Route path="/list-products" render={(props) => <AppLayout {...props} content={ <AdminListProducts /> } />} />
            <Route path="/list-users" render={(props) => <AppLayout {...props} content={ <AdminListUsers /> } />} />
        </Switch>
    </Router>
);