import { Meteor } from 'meteor/meteor'
import React from 'react'
import ReactDOM from 'react-dom'
import { routes } from '../imports/startup/client/routes.js'
import Materialize from 'materialize-css'

if (Meteor.isClient) {

    // Inicializamos Materialize de forma global para hacer responsive la aplicacion
    // Global Materialize initialization to make the application responsive
    global.M = Materialize;
    global.Materialize = Materialize;
}

Meteor.startup(() => {

    // Ejecutamos react-dom especificando el punto de entrada del renderizado
    // Run the react-dom render specifying the entry point
    ReactDOM.render(routes, document.getElementById('react-root'));

    // Inicializamos la funcion de la barra lateral de navegacion
    // Sidenav bar function initialization
    $('.sidenav').sidenav();
});